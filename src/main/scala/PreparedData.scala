
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._

trait PreparedData {

  val JDBC_URL = "jdbc:mysql://localhost:32768/PAYMENTS"
  val DRIVER = "com.mysql.cj.jdbc.Driver"
  val USER = "root"


  val spark = SparkSession.builder().appName("Payment predict").master("local[*]").getOrCreate()
  val reader = spark.read.format("jdbc").option("url", JDBC_URL).
    option("driver", DRIVER).option("user", USER).option("database", "PAYMENTS")

  import spark.implicits._

  val toDouble = udf[Double, String](_.toDouble)

  def prepareData:Dataset[Row] = {
  //Prepare data
  val invoices = reader.option("dbtable", "invoice").load
    .filter(col("paid_status") =!= "UNPAID")
    .filter(col("is_deleted") === false)
    .withColumnRenamed("id", "invoice_id")
    .withColumnRenamed("created_at", "invoice_created").cache()

  val payments_t = reader.option("dbtable", "payment").load
    .withColumnRenamed("id", "payment_id")
    .withColumnRenamed("created_at", "payment_created")
  // select customer payments count
  val customerPaymentsCount = payments_t.select($"customer_id")
    .where($"payment_created" > date_sub($"payment_created", 90))
    .groupBy($"customer_id").count()
    .withColumnRenamed("count", "customer_payment_ct")

  // select customers total payment amount
  val customerPayTotal = payments_t
    .where($"payment_created" > date_sub($"payment_created", 90))
    .groupBy($"customer_id").sum("amount")
    .withColumnRenamed("sum(amount)", "customer_spent")

  val payments = payments_t.drop("customer_id").cache()

  val payment_invoice = reader.option("dbtable", "payment_invoice").load
    .select("payment_id", "invoice_id")

  //Join and select required fields
  val combined = invoices.join(payment_invoice, "invoice_id")
    .join(payments, "payment_id")
    .select("payment_id", "invoice_id", "customer_id", "total", "due",
      "invoice_created", "amount", "payment_created")
    .withColumn("paid_in_days", datediff(col("payment_created"), col("invoice_created")))
    .withColumn("due_to_days", datediff(col("due"), col("invoice_created")))
    .join(customerPaymentsCount, "customer_id")
    .join(customerPayTotal, "customer_id").cache()

    //Calculate avg pay day
    val customerAvgPayDate = combined.groupBy($"customer_id").avg("paid_in_days").withColumnRenamed("avg(paid_in_days)", "avg_paid_in_days")

    //Join avg. Take first payment
    val data = combined.join(customerAvgPayDate, "customer_id").drop("due", "invoice_created", "invoice_id")
      .groupBy($"customer_id", $"total", $"amount", $"due_to_days", $"customer_payment_ct",
        $"customer_spent", $"avg_paid_in_days")
      .agg(min($"payment_id"), min($"payment_created") as "payment_created", min($"paid_in_days") as "paid_in_days" )
    // select distinct
     data.repartition().distinct().drop("payment_created", "min(payment_id)").withColumn("total", toDouble(col("total"))).cache()
  }
}
