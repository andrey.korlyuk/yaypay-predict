import org.apache.spark.ml.tuning.CrossValidatorModel
import org.apache.spark.sql.SparkSession

object Main {

  val spark = SparkSession.builder().appName("Payment predict").master("local[*]").getOrCreate()

  def main(args: Array[String]): Unit = {
    val days = predictDate(25479, 5000, 20, 50000, 30, 5)
    val amount = predictAmount(25479, 5000, 20, 50000, 30, days.toInt, 5)
    println(amount + "USD in " + days+ " days")
  }

  //predict amount
  def predictAmount(id: Int, invoiceTotal: Double, payment_ct: Int, spent_total: Double, invoiceValid: Int, paidInDays: Int, avgPaidDays: Double): Double ={

    val predict = spark.createDataFrame(Seq((id, invoiceTotal, payment_ct, spent_total, invoiceValid, paidInDays, avgPaidDays)))
      .toDF("customer_id", "total", "customer_payment_ct", "customer_spent", "due_to_days", "paid_in_days", "avg_paid_in_days")
    val cv = CrossValidatorModel.load("amount-model")
    cv.transform(predict).select("prediction").collect().head.getDouble(0)
  }

  //Predict date of payment
  def predictDate(id: Int, invoiceTotal: Double, payment_ct: Int, spent_total: Double, invoiceValid: Int, avgPaidDays: Double): Double ={

    val predict = spark.createDataFrame(Seq((id, invoiceTotal, payment_ct, spent_total, invoiceValid, avgPaidDays)))
      .toDF("customer_id", "total", "customer_payment_ct", "customer_spent", "due_to_days", "avg_paid_in_days")
    val cv = CrossValidatorModel.load("date-model")
    cv.transform(predict).select("prediction").collect().head.getDouble(0)
  }
}
