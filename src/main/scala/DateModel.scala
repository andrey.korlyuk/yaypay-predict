import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.{RandomForestRegressionModel, RandomForestRegressor}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.sql.functions._


object DateModel extends PreparedData{

  def main(args: Array[String]) = {
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)

    val fullData = prepareData.withColumn("label", toDouble(col("paid_in_days"))).limit(4000).cache()
    fullData.show()

    val featureAssembler = new VectorAssembler()
      .setOutputCol("features")
      .setInputCols(Array("customer_id", "total", "customer_payment_ct", "customer_spent", "avg_paid_in_days"))


    val glr = new RandomForestRegressor()
      .setNumTrees(14)
      .setMinInfoGain(0.2)

    val pipeline = new Pipeline().setStages(Array(featureAssembler, glr))

    val paramgrid = new ParamGridBuilder()
        .addGrid(glr.maxDepth, Array(9, 10, 11, 12))
        .addGrid(glr.minInfoGain, Array(0.1, 0.2))
        .addGrid(glr.numTrees, Array( 13, 14, 15))
      .build()

    val validator = new CrossValidator().setEstimator(pipeline)
      .setEvaluator(new RegressionEvaluator())
      .setEstimatorParamMaps(paramgrid)
      .setNumFolds(2)

    val splits = fullData.randomSplit(Array(0.7, 0.3))
    val (data_training, data_testing) = (splits(0), splits(1))

    val cvModel = validator.fit(data_training)

    val predicted = cvModel.transform(data_testing).select("prediction", "label")
    predicted.show()
    val metrics = new RegressionMetrics(predicted.rdd.map(r => (r.getDouble(0), r.getDouble(1))))
    // Squared error
    printf("MSE = %f\n", metrics.meanSquaredError)
    printf("RMSE = %f\n", metrics.rootMeanSquaredError)
    // R-squared
    printf("R Squared = %f\n", metrics.r2)
    // Mean absolute error
    printf("MAE = %f\n", metrics.meanAbsoluteError)
    // Explained variance
    printf("Explained Variance = %f\n", metrics.explainedVariance)

    // what are the best parameters?
    val bestPipelineModel = cvModel.bestModel.asInstanceOf[PipelineModel]
    val stages = bestPipelineModel.stages
    val randomForest = stages(1).asInstanceOf[RandomForestRegressionModel]
    printf("maxDepth = %d\n", randomForest.getMaxDepth)
    printf("minInfoGain = %f\n", randomForest.getMinInfoGain)
    printf("numTrees = %d\n", randomForest.getNumTrees)

    cvModel.write.overwrite().save("date-model")

    spark.close
  }
}
